# Feuille de style du blog Shaft Inc.

Ce dépôt a juste pour ambition de stocker les différentes versions de la feuille de style [de mon blog](https://www.shaftinc.fr/). Cela documente au passage mes quelques progrès en CSS (et indirectement les progrès dans la compréhension de la sémantique de HTML5).

N'ayant pas gardé d'archives de celle-ci à chaque modification, les vieilles versions ont été récupérées via [la Wayback Machine](https://web.archive.org). Il manque par conséquent peut-être quelques versions mineures.
