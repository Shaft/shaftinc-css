- 2023-02-22 : renommage de `feed` en `menu` suite à l'ajout du pictogramme Mastodon
- 2023-01-19 : ajout de la classe `feed` pour gérer la mise en forme spécifique du symbole de flux de syndication
- 2022-11-15 : ajout de la police Hack Bold Subset (sous le nom Hack Bold) pour les quelques cas où le gras est nécessaire pour des sorties de terminal. Normalement, le sous-ensemble inclus et suffisant — et permet de me pas trop alourdir la charge réseau.
- 2022-09-15 : pour les pages d'index (ex : https://www.shaftinc.fr/misc/), les `td` restent alignés à gauche
- 2022-09-08 : changement de l'alignement pour le texte des éléments `td` des tableaux (le 1er à gauche, le reste centré)

## [2.2] 2022-08-10
- 2022-08-10 : Ajout d'une police "Symboles" créée manuellement pour les caractères particuliers nécessaires (2 caractères présents lors de la mise en ligne : l'espace insécable et la flèche U+2AB9).
- 2022-02-07 : Petits ajustements

## [2.1] 2022-02-02
- 2022-02-02 : Ajout de la police "Encode Sans Narrow SemiBold" pour un affichage correct des textes en gras. Animation du trait sous les liens hypertextes lors d'un `hover` ou d'un `focus`. Suppression de la media query `speech` dépréciée dans les spécifications Media Queries Level 4
- 2021-09-06 : ajout de `scroll-padding-top` pour bien caler l'affichage lors de l'utilisation de fragments dans les URL
- 2021-02-01 : Restriction thème sombre à l'écran seulement (`media **only screen**`)
- 2020-11-16 : Ajustement de la position de la flèche en avant le titre des articles

## [2.0] 2020-11-14
- 2020-11-14 : Ajout du thème sombre. Changement du comportement `hover` pour les liens hypertextes
- 2019-11-11 : Correctif pour les entêtes `h4` sur les articles

## [1.3] 2019-11-11
- 2019-11-11 : Ajout de la flèche devant les titres sur la page de garde. Nouveau changement de la couleur des liens hypertextes
- 2019-09-14 : Mise en page. Améliorations sur l'impression. Ajout de la media query `speech`
- 2019-08-20 : Ajout de la police de la famille Palatino pour l'impression
- 2019-04-14 : Changement de couleur des liens hypertextes. Correction du nom de la police. Petits ajustements
- 2018-06-02 : Ajout classe `.sql` pour afficher correctement le résultat d'une requête dans un billet
- 2018-03-02 : Suppression de la classe `.smallimg`

## [1.2] 2018-02-16
- 2018-02-16 : Suppression de la plupart des classes pour utiliser au mieux la sémantique HTML5. Changement de la police Ubuntu pour Encode Sans Narrow Light
- 2018-01-08 : Ajout de polices de caractères propres (Hack et Ubuntu) via `@font-face`. Ajout d'une licence. Petits ajustements

## [1.1] 2017-09-02
- 2017-09-02 : Ajout des media queries `media only screen` et `media only print`. Suppression de classes inutiles (`.error`, `.normal`, `.note`)
- 2017-04-15 : Petits ajustements de mise en pages. Ajout des classes `.error` et `.normal` (??)
- 2017-03-18 : Refontes des entêtes `h1`, `h2`... et des tables
- 2016-11-26 : Ajout d'une police de caratères pour les éléments `pre` et `code`

## [1.0] 2016-01-07
- Plus vieille version retrouvée mais a priori pas la plus vieille ayant tournée en prod
- La plus basique
- La plus « empruntée » également
- Un des principaux ajouts comparé à le feuille de style de calomel.org est la gestion des tables
